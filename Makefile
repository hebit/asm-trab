all:
	nasm -f elf -d ELF_TYPE asm_io.asm
	nasm -f elf code.asm
	gcc -o script driver.c code.o asm_io.o -m32

clean:
	rm -f *.lst
	rm -f *.o